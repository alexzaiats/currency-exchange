package com.test.currency.exchange.base.adapter

interface IDataAdapter<D> {

    fun setData (data: D)

    fun getData(): D
}