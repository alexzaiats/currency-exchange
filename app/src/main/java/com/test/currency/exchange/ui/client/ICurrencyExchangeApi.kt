package com.test.currency.exchange.ui.client

import com.test.currency.exchange.ui.model.CurrencyRatesDTO

interface ICurrencyExchangeApi {

    fun getCurrencyRates(): CurrencyRatesDTO
}