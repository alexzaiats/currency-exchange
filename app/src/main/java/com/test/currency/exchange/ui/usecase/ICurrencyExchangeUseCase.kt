package com.test.currency.exchange.ui.usecase

import com.test.currency.exchange.base.usecase.observer.UseCaseObserver
import com.test.currency.exchange.base.usecase.subscription.Subscription
import com.test.currency.exchange.ui.model.CurrencyRatesDTO

interface ICurrencyExchangeUseCase {

    fun subscribeDelayedCurrencyRatesUpdates(observer: UseCaseObserver<CurrencyRatesDTO>): Subscription

    fun subscribeImmediatelyCurrencyRatesUpdates(observer: UseCaseObserver<CurrencyRatesDTO>): Subscription

    fun getCachedCurrencyRates(): CurrencyRatesDTO?

    fun getLastRetrievalTime(): Long
}