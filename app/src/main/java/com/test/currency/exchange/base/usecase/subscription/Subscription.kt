package com.test.currency.exchange.base.usecase.subscription

/**
 * Wrapper for subscription to prevent libraries
 * bonding between layers. Presenter will not know with what subscription it works.
 * It could be RxSubscription, LiveData Subscription or any other implementation
 */
interface Subscription {

    fun isActive(): Boolean

    fun unsubscribe()
}