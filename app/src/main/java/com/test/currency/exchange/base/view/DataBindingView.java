package com.test.currency.exchange.base.view;

import android.view.View;
import androidx.annotation.LayoutRes;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.test.currency.exchange.BR;

public class DataBindingView<B extends ViewDataBinding, ViewModel, ActionListener> extends BaseDataView<ViewModel, ActionListener> {

    protected B binding;

    public DataBindingView(@LayoutRes int layoutId) {
        super(layoutId);
    }

    @Override
    public void initViewComponents(View rootView) {
        super.initViewComponents(rootView);
        binding = DataBindingUtil.bind(rootView);
    }

    @Override
    public void displayData(ViewModel data) {
        binding.setVariable(BR.viewModel, data);
        binding.executePendingBindings();
    }

    @Override
    public void setActionsListener(ActionListener actionsListener) {
        super.setActionsListener(actionsListener);
        binding.setVariable(BR.listener, actionsListener);
        binding.executePendingBindings();
    }
}