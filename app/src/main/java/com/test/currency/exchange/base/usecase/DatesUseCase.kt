package com.test.currency.exchange.base.usecase

import android.text.format.DateUtils
import java.util.*
import javax.inject.Inject


class DatesUseCase
@Inject
constructor() {

    fun getRelativeTimeSpanString(fromTime: Long) = DateUtils.getRelativeTimeSpanString(
        fromTime,
        Calendar.getInstance().timeInMillis,
        DateUtils.MINUTE_IN_MILLIS
    ).toString()
}