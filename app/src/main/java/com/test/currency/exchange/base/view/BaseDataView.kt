package com.test.currency.exchange.base.view

import androidx.annotation.LayoutRes


abstract class BaseDataView<VM, L> : BaseView<L>, IDataView<VM> {

    constructor()

    constructor(@LayoutRes layoutId: Int) : super(layoutId)
}