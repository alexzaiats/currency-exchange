package com.test.currency.exchange.base.usecase.preferences

import android.content.Context
import android.content.SharedPreferences
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.test.currency.exchange.ui.model.CurrencyRatesDTO
import javax.inject.Inject

class PreferenceUseCase
@Inject
constructor(val objectMapper: ObjectMapper,
            val context: Context) {

    private val PREFERENCES_APP = "app"
    private val KEY_RATES = "rates"
    private val KEY_TIME = "time"

    private fun getAppPreferences(): SharedPreferences = context.getSharedPreferences(PREFERENCES_APP, Context.MODE_PRIVATE);


    fun getCachedRates(): CurrencyRatesDTO? {
        val json = getAppPreferences().getString(KEY_RATES, null)
        return if (json == null) {
            null
        } else {
            objectMapper.readValue(json)
        }
    }

    fun saveRates(currencyRatesDTO: CurrencyRatesDTO) {
        getAppPreferences()
            .edit()
            .putString(KEY_RATES, objectMapper.writeValueAsString(currencyRatesDTO))
            .apply()
    }

    fun getLastRetrievalTime(): Long = getAppPreferences().getLong(KEY_TIME, 0)

    fun setLastRetrievalTime(time: Long) {
        getAppPreferences()
            .edit()
            .putLong(KEY_TIME, time)
            .apply()
    }
}