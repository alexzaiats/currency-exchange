package com.test.currency.exchange.base.dagger

import android.content.Context
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.test.currency.exchange.ExchangeApplication
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient

@Module
class ApplicationModule(val application: ExchangeApplication) {

    @Provides
    fun provideContext(): Context = application

    @Provides
    fun provideOkHttpClient(): OkHttpClient = OkHttpClient()

    @Provides
    fun provideObjectMapper(): ObjectMapper = ObjectMapper()
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
}