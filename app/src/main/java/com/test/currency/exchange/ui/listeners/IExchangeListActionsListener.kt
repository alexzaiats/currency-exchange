package com.test.currency.exchange.ui.listeners

interface IExchangeListActionsListener {

    fun onSelected()

    fun onValueChanged(value: String)
}