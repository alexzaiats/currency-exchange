package com.test.currency.exchange.ui

import com.test.currency.exchange.base.dagger.BaseActivityFeatureModule
import com.test.currency.exchange.ui.listeners.IExchangeActionsListener
import com.test.currency.exchange.ui.presenter.ExchangePresenter
import com.test.currency.exchange.ui.usecase.CurrencyExchangeUseCase
import com.test.currency.exchange.ui.usecase.ICurrencyExchangeUseCase
import com.test.currency.exchange.ui.view.ExchangeView
import com.test.currency.exchange.ui.view.IExchangeView
import dagger.Module
import dagger.Provides

@Module
class ExchangeModule: BaseActivityFeatureModule<ExchangeActivity>() {

    @Provides
    fun provideActionsListener(presenter: ExchangePresenter): IExchangeActionsListener = presenter

    @Provides
    fun provideView(view: ExchangeView): IExchangeView = view

    @Provides
    fun provideUseCase(useCase: CurrencyExchangeUseCase): ICurrencyExchangeUseCase = useCase
}