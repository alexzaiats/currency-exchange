package com.test.currency.exchange.base.adapter;

import androidx.annotation.NonNull;
import com.annimon.stream.function.Function;
import com.test.currency.exchange.base.transformation.Transformator;

public class Subadapter<Domain, ViewModel, ActionListener, SuperDomain> {
    @NonNull
    private final Function<SuperDomain, Boolean> toViewType;
    @NonNull
    private final Transformator<Domain, ViewModel, ActionListener> transformator;

    private Subadapter(
            @NonNull Function<SuperDomain, Boolean> toViewType,
            @NonNull Transformator<Domain, ViewModel, ActionListener> transformator) {
        this.toViewType = toViewType;
        this.transformator = transformator;
    }

    public static <Domain, ViewModel, ActionListener, SuperDomain> Subadapter<Domain, ViewModel, ActionListener, SuperDomain> create(
            @NonNull Function<SuperDomain, Boolean> toViewType,
            @NonNull Transformator<Domain, ViewModel, ActionListener> viewBinderAndCreator) {
        return new Subadapter<>(toViewType, viewBinderAndCreator);
    }

    @NonNull
    public Function<SuperDomain, Boolean> toViewType() {
        return toViewType;
    }

    @NonNull
    public Transformator<Domain, ViewModel, ActionListener> transformator() {
        return transformator;
    }

    public static <Domain, ViewModel, ActionListener, SuperDomain> Subadapter<Domain, ViewModel, ActionListener, SuperDomain> create(
            @NonNull Class<Domain> aClass,
            @NonNull Transformator<Domain, ViewModel, ActionListener> transformator
    ) {
        return create(aClass::isInstance, transformator);
    }

}