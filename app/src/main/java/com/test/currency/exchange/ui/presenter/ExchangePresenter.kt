package com.test.currency.exchange.ui.presenter

import android.util.Log
import com.test.currency.exchange.base.dagger.scope.ScreenScope
import com.test.currency.exchange.base.presenter.BasePresenter
import com.test.currency.exchange.base.usecase.DatesUseCase
import com.test.currency.exchange.base.usecase.observer.UseCaseObserver
import com.test.currency.exchange.base.usecase.subscription.Subscription
import com.test.currency.exchange.ui.adapter.CurrencyExchangeAdapter
import com.test.currency.exchange.ui.converter.RatesDTOConverter
import com.test.currency.exchange.ui.listeners.IExchangeActionsListener
import com.test.currency.exchange.ui.model.CurrencyRatesDTO
import com.test.currency.exchange.ui.model.ExchangeCurrency
import com.test.currency.exchange.ui.usecase.ICurrencyExchangeUseCase
import com.test.currency.exchange.ui.view.IExchangeView
import javax.inject.Inject

@ScreenScope
class ExchangePresenter
@Inject
constructor(
    val adapter: CurrencyExchangeAdapter,
    val useCase: ICurrencyExchangeUseCase,
    val converter: RatesDTOConverter,
    val datesUseCase: DatesUseCase
) : BasePresenter<IExchangeView>(), IExchangeActionsListener {

    lateinit var subscription: Subscription

    private var previousCurrencyRatesDTO: CurrencyRatesDTO? = null
    private var currencySelectedCountMap = HashMap<String, Int>()
    private var selectedCurrencyWeight = 1

    override fun setView(view: IExchangeView) {
        super.setView(view)
        val cachedData = useCase.getCachedCurrencyRates()
        if (cachedData != null) {
            adapter.setData(converter.convert(cachedData))
        }
    }

    override fun onViewIsAvailableForInteraction() {
        super.onViewIsAvailableForInteraction()
        subscription = useCase.subscribeImmediatelyCurrencyRatesUpdates(object : UseCaseObserver<CurrencyRatesDTO> {
            override fun onData(data: CurrencyRatesDTO) {
                getView()?.hideConnectionLabel()
                val shouldInitSelectedCountMap = previousCurrencyRatesDTO == null

                if (data != previousCurrencyRatesDTO) {
                    previousCurrencyRatesDTO = data
                    val list = converter.convert(data)
                    if (shouldInitSelectedCountMap) {
                        currencySelectedCountMap[list[0].code] = 1
                    }
                    val sortedList = list.sortedWith(compareByDescending { currencySelectedCountMap[it.code] ?: 0 })
                    adapter.setData(sortedList)
                }
            }

            override fun onError(error: Throwable) {
                getView()?.showConnectionErrorLabel(datesUseCase.getRelativeTimeSpanString(useCase.getLastRetrievalTime()))
                Log.e("Error", Log.getStackTraceString(error))
                subscription.unsubscribe()
                subscription = useCase.subscribeDelayedCurrencyRatesUpdates(this)
            }
        })
    }

    override fun onViewIsUnavailableForInteraction() {
        super.onViewIsUnavailableForInteraction()
        subscription.unsubscribe()
    }

    override fun onCurrencySelected(currency: ExchangeCurrency) {
        currencySelectedCountMap[currency.code] = ++selectedCurrencyWeight
    }
}