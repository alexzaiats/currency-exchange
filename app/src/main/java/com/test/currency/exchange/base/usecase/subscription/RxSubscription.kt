package com.test.currency.exchange.base.usecase.subscription

import io.reactivex.disposables.Disposable


class RxSubscription(private val disposable: Disposable) : Subscription {

    override fun unsubscribe() {
        disposable.dispose();
    }

    override fun isActive() = !disposable.isDisposed;
}