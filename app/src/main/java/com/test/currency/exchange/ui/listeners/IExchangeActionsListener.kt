package com.test.currency.exchange.ui.listeners

import com.test.currency.exchange.ui.model.ExchangeCurrency

interface IExchangeActionsListener {

    fun onCurrencySelected(currency: ExchangeCurrency)
}