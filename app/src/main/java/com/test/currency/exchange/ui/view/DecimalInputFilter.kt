package com.test.currency.exchange.ui.view

import android.text.InputFilter
import android.text.Spanned


class DecimalInputFilter : InputFilter {

    private val maxDecimalLength = 2

    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence? {

        val builder = StringBuilder(dest)
        builder.insert(dstart, source)
        val temp = builder.toString()

        if (temp == ".") {
            return "0."
        }

        //after zero only dot is allowed
        if (temp.startsWith("0") && temp.length > 1 && temp[1] != '.') {
            return ""
        }

        var dotPos = -1
        val len = dest.length
        for (i in 0 until len) {
            val c = dest[i]
            if (c == '.' || c == ',') {
                dotPos = i
                break
            }
        }

        if (dotPos >= 0) {
            // protects against many dots
            if (source == "." || source == ",") {
                return ""
            }
            // if the text is entered before the dot
            if (dend <= dotPos) {
                return null
            }
            if (len - dotPos > maxDecimalLength) {
                return ""
            }
        }

        return null
    }
}