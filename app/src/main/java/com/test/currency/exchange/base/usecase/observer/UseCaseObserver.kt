package com.test.currency.exchange.base.usecase.observer

interface UseCaseObserver<D> {

    fun onData(data: D)

    fun onError(error: Throwable)
}