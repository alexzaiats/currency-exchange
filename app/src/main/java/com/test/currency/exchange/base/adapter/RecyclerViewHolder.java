package com.test.currency.exchange.base.adapter;

import androidx.recyclerview.widget.RecyclerView;
import com.test.currency.exchange.base.view.BaseDataView;

public class RecyclerViewHolder<View extends BaseDataView> extends RecyclerView.ViewHolder {

    public View view;

    public RecyclerViewHolder(View v) {
        super(v.getRootView());
        this.view = v;
    }
}