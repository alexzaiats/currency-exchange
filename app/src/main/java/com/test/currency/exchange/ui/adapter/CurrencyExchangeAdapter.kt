package com.test.currency.exchange.ui.adapter

import android.content.Context
import androidx.core.content.ContextCompat
import com.test.currency.exchange.base.adapter.MVPAdapter
import com.test.currency.exchange.base.adapter.Subadapter
import com.test.currency.exchange.base.adapter.Subadapter.create
import com.test.currency.exchange.base.dagger.scope.ScreenScope
import com.test.currency.exchange.base.extensions.tryParseDouble
import com.test.currency.exchange.base.transformation.Transformator
import com.test.currency.exchange.ui.listeners.IExchangeActionsListener
import com.test.currency.exchange.ui.listeners.IExchangeListActionsListener
import com.test.currency.exchange.ui.model.ExchangeCurrency
import com.test.currency.exchange.ui.view.CurrencyExchangeListItem
import com.test.currency.exchange.ui.viewmodel.ExchangeCurrencyViewModel
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import javax.inject.Inject
import javax.inject.Provider

@ScreenScope
class CurrencyExchangeAdapter
@Inject
constructor(
    private val context: Context,
    private val actionsListenerProvider: Provider<IExchangeActionsListener>
) : MVPAdapter<ExchangeCurrency>() {

    private var selectedUsdValue: Double = 1.0
    private var selectedCurrencyCode: String = "usd"

    private val df: DecimalFormat =
        DecimalFormat("#.##", DecimalFormatSymbols(Locale.US)) //separate double only with the dot, ignoring commas

    init {
        df.roundingMode = RoundingMode.CEILING
    }

    override fun subadapters(): List<Subadapter<*, *, *, ExchangeCurrency>> {
        return Arrays.asList(
            create(
                { item -> item is ExchangeCurrency },
                Transformator.Functional<ExchangeCurrency, ExchangeCurrencyViewModel, IExchangeListActionsListener>(
                    { CurrencyExchangeListItem() },
                    { item, _ ->
                        ExchangeCurrencyViewModel(
                            name = item.name,
                            code = item.code,
                            value = df.format((selectedUsdValue * item.usdExchangeRate)),
                            icon = ContextCompat.getDrawable(context, item.icon)
                        )
                    }
                ) { exchangeCurrency ->
                    object : IExchangeListActionsListener {

                        override fun onSelected() {
                            selectedCurrencyCode = exchangeCurrency.code
                            actionsListenerProvider.get().onCurrencySelected(exchangeCurrency)
                            val currentPosition = findIndexByCurrencyCode(exchangeCurrency.code)
                            items.removeAt(currentPosition).also {
                                items.add(0, it)
                            }
                            notifyItemMoved(currentPosition, 0)
                        }

                        override fun onValueChanged(value: String) {
                            selectedUsdValue = value.tryParseDouble() / exchangeCurrency.usdExchangeRate
                            notifyItemRangeChanged(1, itemCount - 1)
                        }
                    }
                }
            )
        )
    }

    override fun setData(data: List<ExchangeCurrency>) {
        if (items?.isNotEmpty() == true) {
            items.clear()
            items.addAll(data)
            notifyItemRangeChanged(1, itemCount - 1)
        } else {
            super.setData(data)
        }
    }

    /**
     *  it is not safe to use items.indexOf() because data list
     *  always changing, so we cannot be sure that search by equals will found required item.
     *  Currency codes are static, so search by code always will return correct result.
     */
    private fun findIndexByCurrencyCode(code: String): Int {
        items.forEachIndexed { index, exchangeCurrency ->
            if (exchangeCurrency.code == code) return index
        }
        return -1
    }
}
