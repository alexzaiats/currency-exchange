package com.test.currency.exchange.ui.viewmodel

import android.graphics.drawable.Drawable

data class ExchangeCurrencyViewModel(val name: String,
                                     val code: String,
                                     val icon: Drawable?,
                                     val value: String)