package com.test.currency.exchange.ui.view

import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.test.currency.exchange.R
import com.test.currency.exchange.base.dagger.scope.ScreenScope
import com.test.currency.exchange.base.view.DataBindingView
import com.test.currency.exchange.databinding.ViewExchangeBinding
import com.test.currency.exchange.ui.adapter.CurrencyExchangeAdapter
import com.test.currency.exchange.ui.listeners.IExchangeActionsListener
import javax.inject.Inject

@ScreenScope
class ExchangeView
@Inject
constructor(val adapter: CurrencyExchangeAdapter) : DataBindingView<ViewExchangeBinding, Any, IExchangeActionsListener>(R.layout.view_exchange), IExchangeView {

    override fun initViewComponents(rootView: View) {
        super.initViewComponents(rootView)
        binding.apply {
            recyclerView.apply {
                adapter = this@ExchangeView.adapter
                layoutManager = LinearLayoutManager(viewContext)
                itemAnimator = DefaultItemAnimator().apply { supportsChangeAnimations = false }
            }
        }
    }

    override fun showConnectionErrorLabel(lastRetrievalTime: String) {
        binding.errorLabel.text = viewContext.getString(R.string.error_label, lastRetrievalTime)
        binding.errorLabel.visibility = View.VISIBLE
    }

    override fun hideConnectionLabel() {
        binding.errorLabel.visibility = View.GONE
    }
}