package com.test.currency.exchange.base.dagger

import com.test.currency.exchange.ExchangeApplication
import dagger.Component
import dagger.android.AndroidInjectionModule

@Component(modules = [ApplicationModule::class, ExchangeBindingModule::class, AndroidInjectionModule::class, ApiModule::class])
interface ExchangeComponent {

    fun inject(application: ExchangeApplication)
}