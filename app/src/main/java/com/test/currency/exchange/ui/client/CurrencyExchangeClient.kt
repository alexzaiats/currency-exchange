package com.test.currency.exchange.ui.client

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.test.currency.exchange.base.client.ClientConstants
import com.test.currency.exchange.ui.model.CurrencyRatesDTO
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONObject
import javax.inject.Inject


class CurrencyExchangeClient
@Inject
constructor(
    val okHttpClient: OkHttpClient,
    val objectMapper: ObjectMapper
) : ICurrencyExchangeApi {

    val PATH_CURRENCY_EXCHANGE = "/latest?base=USD"

    override fun getCurrencyRates(): CurrencyRatesDTO {
        val request = Request.Builder()
            .url(ClientConstants.BASE_URL + PATH_CURRENCY_EXCHANGE)
            .build()

        return okHttpClient
            .newCall(request)
            .execute()
            .use { response ->
                val json = response.body()?.string().orEmpty()
                val ratesJson = JSONObject(json).optJSONObject("rates").toString()
                objectMapper.readValue(ratesJson)
            }
    }
}