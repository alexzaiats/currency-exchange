package com.test.currency.exchange.base.dagger.scope

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ScreenScope