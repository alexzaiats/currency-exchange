package com.test.currency.exchange.base.dagger

import android.content.Context
import androidx.fragment.app.FragmentActivity
import com.test.currency.exchange.base.qualifiers.ViewContext
import dagger.Module
import dagger.Provides

@Module
class ActivityInjectorModule {

    @Provides
    @ViewContext
    fun provideViewContext(activity: FragmentActivity): Context = activity
}