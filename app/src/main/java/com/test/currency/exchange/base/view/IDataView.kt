package com.test.currency.exchange.base.view

interface IDataView<D> {

    fun displayData(data: D)

}