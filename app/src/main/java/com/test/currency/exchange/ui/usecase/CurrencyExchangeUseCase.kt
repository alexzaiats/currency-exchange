package com.test.currency.exchange.ui.usecase

import com.test.currency.exchange.base.dagger.scope.ScreenScope
import com.test.currency.exchange.base.usecase.observer.UseCaseObserver
import com.test.currency.exchange.base.usecase.preferences.PreferenceUseCase
import com.test.currency.exchange.base.usecase.subscription.RxSubscription
import com.test.currency.exchange.base.usecase.subscription.Subscription
import com.test.currency.exchange.ui.client.ICurrencyExchangeApi
import com.test.currency.exchange.ui.model.CurrencyRatesDTO
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@ScreenScope
class CurrencyExchangeUseCase
@Inject
constructor(
    private val client: ICurrencyExchangeApi,
    private val preferenceUseCase: PreferenceUseCase
) : ICurrencyExchangeUseCase {

    override fun subscribeDelayedCurrencyRatesUpdates(observer: UseCaseObserver<CurrencyRatesDTO>): Subscription {
        return subscribeCurrencyRatesUpdates(observer, 1)
    }

    override fun subscribeImmediatelyCurrencyRatesUpdates(observer: UseCaseObserver<CurrencyRatesDTO>): Subscription {
        return subscribeCurrencyRatesUpdates(observer, 0)
    }

    private fun subscribeCurrencyRatesUpdates(
        observer: UseCaseObserver<CurrencyRatesDTO>,
        startWith: Long
    ): Subscription {
        return RxSubscription(
            Observable
                .interval(1, TimeUnit.SECONDS)
                .startWith(startWith)
                .map { client.getCurrencyRates() }
                .map {
                    preferenceUseCase.saveRates(it)
                    preferenceUseCase.setLastRetrievalTime(System.currentTimeMillis())
                    it
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer::onData, observer::onError)
        )
    }

    override fun getCachedCurrencyRates(): CurrencyRatesDTO? = preferenceUseCase.getCachedRates()

    override fun getLastRetrievalTime(): Long = preferenceUseCase.getLastRetrievalTime()
}