package com.test.currency.exchange.base.presenter

abstract class BasePresenter<V> {

    var isViewAvailableForInteractions: Boolean = false
        protected set

    private var view: V? = null

    open fun setView(view: V) {
        this.view = view
    }

    fun getView(): V? = view

    open fun unbindView() {
        this.view = null
    }

    open fun onViewIsUnavailableForInteraction() {
        isViewAvailableForInteractions = false
    }

    open fun onViewIsAvailableForInteraction() {
        isViewAvailableForInteractions = true
    }
}