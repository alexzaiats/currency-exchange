package com.test.currency.exchange.base.dagger

import com.test.currency.exchange.base.dagger.scope.ScreenScope
import com.test.currency.exchange.ui.ExchangeActivity
import com.test.currency.exchange.ui.ExchangeModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ExchangeBindingModule {

    @ScreenScope
    @ContributesAndroidInjector(modules = [ExchangeModule::class, ActivityInjectorModule::class])
    abstract fun bindExchangeActivity(): ExchangeActivity
}