package com.test.currency.exchange.base.dagger

import android.app.Activity
import androidx.fragment.app.FragmentActivity
import dagger.Module
import dagger.Provides

@Module
abstract class BaseActivityFeatureModule<A> where A: FragmentActivity {

    @Provides
    fun provideActivity(activity: A): Activity = activity

    @Provides
    fun provideFragmentActivity(activity: A): FragmentActivity = activity
}