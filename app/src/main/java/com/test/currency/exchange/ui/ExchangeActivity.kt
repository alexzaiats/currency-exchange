package com.test.currency.exchange.ui

import com.test.currency.exchange.base.activity.MVPActivityBase
import com.test.currency.exchange.ui.presenter.ExchangePresenter
import com.test.currency.exchange.ui.view.ExchangeView
import dagger.android.AndroidInjection

class ExchangeActivity: MVPActivityBase<ExchangeView, ExchangePresenter>() {

    override fun injectMVPComponents() {
        AndroidInjection.inject(this)
    }
}