package com.test.currency.exchange.base.transformation;

import androidx.annotation.NonNull;
import com.annimon.stream.function.BiFunction;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Supplier;
import com.test.currency.exchange.base.view.BaseDataView;

public interface Transformator<Domain, ViewModel, ActionListener> {

    BaseDataView<? extends ViewModel, ? super ActionListener> view();

    ViewModel convert(@NonNull Domain item, int position);

    ActionListener actionListener(@NonNull Domain data);

    class Functional<Domain, ViewModel, ActionListener> implements Transformator<Domain, ViewModel, ActionListener> {

        private final Supplier<BaseDataView<? extends ViewModel, ? super ActionListener>> viewSupplier;
        private final BiFunction<Domain, Integer, ViewModel> converter;
        private final Function<Domain, ActionListener> actionListenerProvider;

        public Functional(
                Supplier<BaseDataView<? extends ViewModel, ? super ActionListener>> viewSupplier,
                BiFunction<Domain, Integer, ViewModel> converter,
                Function<Domain, ActionListener> actionListenerProvider) {
            this.viewSupplier = viewSupplier;
            this.converter = converter;
            this.actionListenerProvider = actionListenerProvider;
        }

        @Override
        public final BaseDataView<? extends ViewModel, ? super ActionListener> view() {
            return viewSupplier.get();
        }

        @Override
        public final ViewModel convert(@NonNull Domain item, int position) {
            return converter.apply(item, position);
        }

        @Override
        public final ActionListener actionListener(@NonNull Domain data) {
            return actionListenerProvider.apply(data);
        }
    }
}
