package com.test.currency.exchange.ui.converter

import android.content.Context
import com.test.currency.exchange.R
import com.test.currency.exchange.base.dagger.scope.ScreenScope
import com.test.currency.exchange.ui.model.CurrencyRatesDTO
import com.test.currency.exchange.ui.model.ExchangeCurrency
import javax.inject.Inject

@ScreenScope
class RatesDTOConverter
@Inject
constructor(val context: Context) {

    val currencyNames = mapOf(
        "usd" to context.getString(R.string.us_dollar),
        "eur" to context.getString(R.string.euro),
        "dkk" to context.getString(R.string.danish_krone),
        "thb" to context.getString(R.string.thai_baht),
        "jpy" to context.getString(R.string.japanese_yen),
        "czk" to context.getString(R.string.croatian_kuna),
        "aud" to context.getString(R.string.australian_dollar),
        "bgn" to context.getString(R.string.bulgarian_lev),
        "brl" to context.getString(R.string.brazilian_real),
        "cad" to context.getString(R.string.canadian_dollar),
        "chf" to context.getString(R.string.swiss_franc),
        "cny" to context.getString(R.string.chinese_yuan),
        "gbp" to context.getString(R.string.british_pound),
        "hkd" to context.getString(R.string.hong_kong_dollar),
        "hrk" to context.getString(R.string.croatian_kuna),
        "huf" to context.getString(R.string.hungarian_forint),
        "idr" to context.getString(R.string.indonesian_rupee),
        "ils" to context.getString(R.string.israel_shekel),
        "inr" to context.getString(R.string.indian_rupee),
        "isk" to context.getString(R.string.iceland_krona),
        "krw" to context.getString(R.string.south_korean_won),
        "mxn" to context.getString(R.string.mexican_peso),
        "myr" to context.getString(R.string.malaysia_ringgit),
        "nzd" to context.getString(R.string.new_zealand_dollar),
        "php" to context.getString(R.string.philippines_peso),
        "pln" to context.getString(R.string.poland_zloty),
        "ron" to context.getString(R.string.romanian_leu),
        "rub" to context.getString(R.string.russian_ruble),
        "sek" to context.getString(R.string.swedish_krona),
        "sgd" to context.getString(R.string.singapore_dollar),
        "try" to context.getString(R.string.turkish_lira),
        "zad" to context.getString(R.string.south_african_rand)
    )

    fun convert(rates: CurrencyRatesDTO): List<ExchangeCurrency> {
        val result = ArrayList<ExchangeCurrency>()
        result.apply {
            add(
                ExchangeCurrency(
                    name = currencyNames["usd"].orEmpty(),
                    icon = R.drawable.ic_usd,
                    code = "usd",
                    usdExchangeRate = 1.0
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["eur"].orEmpty(),
                    icon = R.drawable.ic_eur,
                    code = "eur",
                    usdExchangeRate = rates.eur
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["dkk"].orEmpty(),
                    icon = R.drawable.ic_dkk,
                    code = "dkk",
                    usdExchangeRate = rates.dkk
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["thb"].orEmpty(),
                    icon = R.drawable.ic_thb,
                    code = "thb",
                    usdExchangeRate = rates.thb
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["jpy"].orEmpty(),
                    icon = R.drawable.ic_jpy,
                    code = "jpy",
                    usdExchangeRate = rates.jpy
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["czk"].orEmpty(),
                    icon = R.drawable.ic_czk,
                    code = "czk",
                    usdExchangeRate = rates.czk
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["aud"].orEmpty(),
                    icon = R.drawable.ic_aud,
                    code = "aud",
                    usdExchangeRate = rates.aud
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["bgn"].orEmpty(),
                    icon = R.drawable.ic_bgn,
                    code = "bgn",
                    usdExchangeRate = rates.bgn
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["brl"].orEmpty(),
                    icon = R.drawable.ic_brl,
                    code = "brl",
                    usdExchangeRate = rates.brl
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["cad"].orEmpty(),
                    icon = R.drawable.ic_cad,
                    code = "cad",
                    usdExchangeRate = rates.cad
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["chf"].orEmpty(),
                    icon = R.drawable.ic_chf,
                    code = "chf",
                    usdExchangeRate = rates.chf
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["cny"].orEmpty(),
                    icon = R.drawable.ic_cny,
                    code = "cny",
                    usdExchangeRate = rates.cny
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["gbp"].orEmpty(),
                    icon = R.drawable.ic_gbp,
                    code = "gbp",
                    usdExchangeRate = rates.gbp
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["hkd"].orEmpty(),
                    icon = R.drawable.ic_hkd,
                    code = "hkd",
                    usdExchangeRate = rates.hkd
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["hrk"].orEmpty(),
                    icon = R.drawable.ic_hrk,
                    code = "hrk",
                    usdExchangeRate = rates.hrk
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["huf"].orEmpty(),
                    icon = R.drawable.ic_huf,
                    code = "huf",
                    usdExchangeRate = rates.huf
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["idr"].orEmpty(),
                    icon = R.drawable.ic_idr,
                    code = "idr",
                    usdExchangeRate = rates.idr
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["ils"].orEmpty(),
                    icon = R.drawable.ic_ils,
                    code = "ils",
                    usdExchangeRate = rates.ils
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["inr"].orEmpty(),
                    icon = R.drawable.ic_inr,
                    code = "inr",
                    usdExchangeRate = rates.inr
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["isk"].orEmpty(),
                    icon = R.drawable.ic_isk,
                    code = "isk",
                    usdExchangeRate = rates.isk
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["krw"].orEmpty(),
                    icon = R.drawable.ic_krw,
                    code = "krw",
                    usdExchangeRate = rates.krw
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["mxn"].orEmpty(),
                    icon = R.drawable.ic_mxn,
                    code = "mxn",
                    usdExchangeRate = rates.mxn
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["myr"].orEmpty(),
                    icon = R.drawable.ic_myr,
                    code = "myr",
                    usdExchangeRate = rates.myr
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["nzd"].orEmpty(),
                    icon = R.drawable.ic_nzd,
                    code = "nzd",
                    usdExchangeRate = rates.nzd
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["php"].orEmpty(),
                    icon = R.drawable.ic_php,
                    code = "php",
                    usdExchangeRate = rates.php
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["pln"].orEmpty(),
                    icon = R.drawable.ic_pln,
                    code = "pln",
                    usdExchangeRate = rates.pln
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["ron"].orEmpty(),
                    icon = R.drawable.ic_ron,
                    code = "ron",
                    usdExchangeRate = rates.ron
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["rub"].orEmpty(),
                    icon = R.drawable.ic_rub,
                    code = "rub",
                    usdExchangeRate = rates.rub
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["sek"].orEmpty(),
                    icon = R.drawable.ic_sek,
                    code = "sek",
                    usdExchangeRate = rates.sek
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["sgd"].orEmpty(),
                    icon = R.drawable.ic_sgd,
                    code = "sgd",
                    usdExchangeRate = rates.sgd
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["try"].orEmpty(),
                    icon = R.drawable.ic_try,
                    code = "try",
                    usdExchangeRate = rates.rty_l
                )
            )
            add(
                ExchangeCurrency(
                    name = currencyNames["zad"].orEmpty(),
                    icon = R.drawable.ic_zar,
                    code = "zar",
                    usdExchangeRate = rates.zar
                )
            )
        }
        return result
    }
}