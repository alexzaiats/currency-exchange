package com.test.currency.exchange.base.dagger

import com.test.currency.exchange.ui.client.CurrencyExchangeClient
import com.test.currency.exchange.ui.client.ICurrencyExchangeApi
import dagger.Module
import dagger.Provides

@Module
class ApiModule {

    @Provides
    fun provideExchangeApi(client: CurrencyExchangeClient): ICurrencyExchangeApi = client
}