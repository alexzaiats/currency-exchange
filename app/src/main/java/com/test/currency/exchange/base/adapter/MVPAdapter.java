package com.test.currency.exchange.base.adapter;

import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.test.currency.exchange.base.transformation.Transformator;
import com.test.currency.exchange.base.view.BaseDataView;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Wrapper for RecyclerView.Adapter to support MVP views in adapter.
 * With this adapter view will not know where it is used: in activity as a root view,
 * or just a view for list item.
 * Also it will be much easier to manage multiple view types
 * @param <Domain>
 */
public abstract class MVPAdapter<Domain> extends RecyclerView.Adapter<RecyclerViewHolder<BaseDataView>> implements IDataAdapter<List<Domain>> {

    protected List<Domain> items = new ArrayList<>();
    private List<Subadapter<?, ?, ?, Domain>> subadapters;

    @Override
    public final int getItemViewType(int position) {
        init();
        for (int i = 0; i < subadapters.size(); i++) {
            if (subadapters.get(i).toViewType().apply(getItem(position)))
                return i;
        }
        throw new IllegalStateException(MessageFormat.format("Unexpected by subadapters item [{0}] at position [{1}]", getItem(position), position));
    }

    @NonNull
    private Transformator<?, ?, ?> transformator(int viewType) {
        init();
        if (viewType >= 0 && viewType < subadapters.size()) {
            return subadapters.get(viewType).transformator();
        }
        throw new IllegalArgumentException("Unexpected viewType [" + viewType + "]");
    }

    private void init() {
        if (subadapters == null) {
            subadapters = subadapters();
        }
    }

    @NonNull
    protected abstract List<Subadapter<?, ?, ?, Domain>> subadapters();

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(final int position) {
        return items.get(position).hashCode();
    }

    @NonNull
    public Domain getItem(final int position) {
        return items.get(position);
    }

    @Override
    public final RecyclerViewHolder<BaseDataView> onCreateViewHolder(ViewGroup parent, int viewType) {
        final BaseDataView<?, ?> view = transformator(viewType).view();
        view.initViewComponents(parent.getContext(), parent);
        return new RecyclerViewHolder<>(view);
    }

    @SuppressWarnings("unchecked")
    // we suppress warning as we may deal with multiple viewmodels and listeners in one adapter
    @Override
    public final void onBindViewHolder(RecyclerViewHolder<BaseDataView> holder, int position) {
        final Transformator<Domain, ?, ?> transformator = (Transformator<Domain, ?, ?>) transformator(getItemViewType(position));
        holder.view.displayData(transformator.convert(getItem(position), position));
        holder.view.setActionsListener(transformator.actionListener(getItem(position)));
    }

    @Override
    public List<Domain> getData() {
        return items;
    }

    @Override
    public void setData(List<Domain> items) {
        this.items.clear();
        if (items != null) this.items.addAll(items);
        notifyDataSetChanged();
    }

}