package com.test.currency.exchange.ui.view

interface IExchangeView {

    fun showConnectionErrorLabel(lastRetrievalTime: String)

    fun hideConnectionLabel()
}