package com.test.currency.exchange.ui.model

import androidx.annotation.DrawableRes

data class ExchangeCurrency(val name: String,
                            val code: String,
                            @DrawableRes val icon: Int,
                            val usdExchangeRate: Double)