package com.test.currency.exchange.ui.view

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.test.currency.exchange.R
import com.test.currency.exchange.base.dagger.scope.ScreenScope
import com.test.currency.exchange.base.view.DataBindingView
import com.test.currency.exchange.databinding.ViewExchangeListItemBinding
import com.test.currency.exchange.ui.listeners.IExchangeListActionsListener
import com.test.currency.exchange.ui.viewmodel.ExchangeCurrencyViewModel
import javax.inject.Inject

@ScreenScope
class CurrencyExchangeListItem
@Inject
constructor() :
    DataBindingView<ViewExchangeListItemBinding, ExchangeCurrencyViewModel, IExchangeListActionsListener>(R.layout.view_exchange_list_item) {

    override fun displayData(data: ExchangeCurrencyViewModel?) {
        binding.editText.removeTextChangedListener(watcher)
        super.displayData(data)
        binding.editText.addTextChangedListener(watcher)
    }

    override fun initViewComponents(rootView: View) {
        super.initViewComponents(rootView)
        binding.editText.addTextChangedListener(watcher)
        binding.editText.setOnFocusChangeListener { _, isFocused -> if (isFocused) getActionsListener()?.onSelected() }
        binding.editText.filters = arrayOf(DecimalInputFilter())
    }

    override fun releaseViewComponents() {
        super.releaseViewComponents()
        binding.editText.removeTextChangedListener(watcher)
    }

    private val watcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            getActionsListener()?.onValueChanged(p0.toString())
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }
    }
}