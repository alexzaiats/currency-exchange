package com.test.currency.exchange.base.extensions

fun String.tryParseDouble() : Double {
    return try {
        this.toDouble()
    } catch (e: NumberFormatException) {
        0.0
    }
}