package com.test.currency.exchange.base.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


abstract class BaseView<L> {

    private var actionsListener: L? = null
    var rootView: View? = null
    var layoutId: Int = 0

    open fun setActionsListener(listener: L) {
        actionsListener = listener
    }

    fun getActionsListener() = actionsListener

    val viewContext: Context
        get() = rootView!!.context

    constructor() {
        //nothing to do
    }

    constructor(layoutId: Int) {
        this.layoutId = layoutId
    }

    open fun initViewComponents(context: Context, parent: ViewGroup?) {
        this.initViewComponents(context, parent, false)
    }

    open fun initViewComponents(context: Context, parent: ViewGroup?, attachToRoot: Boolean) {
        if (layoutId == 0) {
            throw IllegalArgumentException("Set layoutId to use initViewComponents(Context, ViewGroup)")
        }
        this.initViewComponents(LayoutInflater.from(context), layoutId, parent, attachToRoot)
    }

    open fun initViewComponents(
        inflater: LayoutInflater, layoutId: Int,
        parent: ViewGroup?,
        attachToRoot: Boolean) {
        this.initViewComponents(inflater.inflate(layoutId, parent, attachToRoot))
    }

    open fun initViewComponents(rootView: View) {
        this.rootView = rootView
    }

    open fun releaseViewComponents() {
        rootView = null
        actionsListener = null
    }
}