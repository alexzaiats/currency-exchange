package com.test.currency.exchange.base.activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import com.test.currency.exchange.base.presenter.BasePresenter;
import com.test.currency.exchange.base.view.BaseView;

import javax.inject.Inject;

/**
 * Base activity for MVP architecture.
 * Injects and links view with presenter.
 * Control lifecycle events for presenter.
 */
public abstract class MVPActivityBase<V extends BaseView, P extends BasePresenter> extends AppCompatActivity {

    @Inject
    protected V viewMVP;

    @Inject
    protected P presenter;

    @Override
    @SuppressWarnings("unchecked")
    protected void onCreate(Bundle savedInstanceState) {
        injectMVPComponents();
        super.onCreate(savedInstanceState);
        initContentView();
        viewMVP.setActionsListener(presenter);
        presenter.setView(viewMVP);
    }

    protected void initContentView() {
        if(viewMVP.getLayoutId() != 0) {
            viewMVP.initViewComponents(this, null);
            setContentView(viewMVP.getRootView());
        } else {
            viewMVP.initViewComponents(getWindow().getDecorView());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onViewIsAvailableForInteraction();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onViewIsUnavailableForInteraction();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getPresenter().unbindView();
    }

    protected P getPresenter() {
        return presenter;
    }

    protected abstract void injectMVPComponents();
}